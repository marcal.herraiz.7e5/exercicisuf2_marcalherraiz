import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Divideix un String
*/

fun main() {
    val sc = Scanner(System.`in`)
    val word = sc.next()
    val character = sc.next().single()

    println(stringSpliter(word,character))
}

fun stringSpliter(word: String?, character: Char): Any {
    return word?.split(character) ?: "Error"
}
