/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Mida d’un String
*/

fun main() {
    val paraula = readln()
    println(paraulaLength(paraula))
}

fun paraulaLength(paraula: String): Int {
    var size = 0
    for (letter in paraula){
        size++
    }
    return size
}
