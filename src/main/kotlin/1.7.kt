/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Eleva’l
*/

fun main() {
    val number1 = readln().toInt()
    val number2 = readln().toInt()

    println(raiseThePowerOf(number1,number2))
}

fun raiseThePowerOf(number1: Int, number2: Int): Int {
    var result = 1
    for (i in 1..number2){
        result*=number1
    }
    return result
}
