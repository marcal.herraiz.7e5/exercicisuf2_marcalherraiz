/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Subseqüència
*/

fun main() {
    val words = readln()
    val number1 = readln().toInt()
    val number2 = readln().toInt()
    println(subSequence(words,number1,number2))
}

fun subSequence(words: String, number1: Int, number2: Int): String {
    var subSequence = ""
    return if (words.length<number1||words.length<number2) "La subseqüència 10-12 de l'String no existe"
    else{
        for (i in number1..number2){
            subSequence += words[i].toString()
        }
        subSequence
    }
}
