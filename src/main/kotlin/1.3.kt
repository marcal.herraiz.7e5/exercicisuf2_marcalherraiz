/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Caràcter d’un String
*/

fun main() {
    val chupapi = readln()
    val number = readln().toInt()

    println(characterInString(chupapi, number))
}

fun characterInString(chupapi: String, number: Int): String {
    return if (chupapi.length<number) "La mida de l'String és inferior a 10"
    else chupapi[number].toString()
}
