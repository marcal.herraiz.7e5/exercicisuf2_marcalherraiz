import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Escriu una línia
*/

fun main() {
    val sc = Scanner(System.`in`)
    val number1 = sc.nextInt()
    val number2 = sc.nextInt()
    val character = sc.next().single()

    println(createLine(number1,number2,character))
}

fun createLine(number1: Int, number2: Int, character: Char): String {
    var line = ""
    for (i in 1..number1){
        line+=" "
    }
    for (i in 1..number2){
        line+=character
    }
    return line
}
