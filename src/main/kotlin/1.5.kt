/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: D’String a MutableList<Char>
*/

fun main() {
    val word = readln()
    println(stringToMutableListOfChar(word))
}

fun stringToMutableListOfChar(word: String): MutableList<Char> {
    val charlist = mutableListOf<Char>()
    for (letter in word){
        charlist.add(letter)
    }
    return charlist
}
