import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Escriu una creu
*/

fun main() {
    val sc = Scanner(System.`in`)
    val number1 = sc.nextInt()
    val character = sc.next().single()

    createCross(number1,character)
}

fun createCross(number1: Int, character: Char) {
    val middle = number1/2

    for (i in 1..middle){
        for (j in 1..middle){
            print(" ")
        }
        println(character)
    }
    repeat(number1){ print(character)}
    println()
    for (i in 1..middle){

        for (j in 1..middle){
            print(" ")
        }
        println(character)
    }
}
