import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: Escriu un marc per un String
*/

fun main() {
    val sc = Scanner(System.`in`)
    val word = sc.nextLine()
    val character = sc.next().single()

    createFrame(word,character)
}

fun createFrame(word: String, character: Char) {

    repeat(word.length+4){ print(character)}
    println()
    print(character)
    repeat(word.length+2){ print(" ")}
    println(character)
    println("$character $word $character")
    print(character)
    repeat(word.length+2){ print(" ")}
    println(character)
    repeat(word.length+4){ print(character)}
    println()
}
