import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/12/14
* TITLE: String és igual
*/

fun main() {
    val sc = Scanner(System.`in`)
    val primeraEntrada = sc.next()
    val segonaEntrada = sc.next()
    println(stringIsTheSame(primeraEntrada,segonaEntrada))
}

fun stringIsTheSame(primeraEntrada: String?, segonaEntrada: String?): Boolean {
    return primeraEntrada==segonaEntrada
}

